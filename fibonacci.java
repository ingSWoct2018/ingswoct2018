/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package fibonacci_wha;

/**
 *
 * @author Angel Wha
 */

import java.util.Scanner;

public class fibonacci {

    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        System.out.print("Ingresa Un Numero Para Calcular La Serie De Fibonacci:\n");
        long num = sc.nextLong();
        Fibonacci_wha fb = new Fibonacci_wha();
        System.out.println("[ITERATIVO] Fibonacci creado:  "+ fb.calcularFibonacciI(num)+"\n");
        System.out.println("[RECURSIVO] Fibonacci creado:  "+ fb.calcularFibonacciR(num)+"\n");
        


    }
}
